/// <reference path="./cpml.d.ts" />
/// <reference path="./dkjson.d.ts" />
/// <reference path="./du-api.d.ts" />
import { Vector } from "cpml/vec3";
import * as Vec3 from "cpml/vec3";
import * as Utils from "cpml/utils";
import * as Constants from "cpml/constants";
import * as PID from "cpml/pid";
import { SGui } from "cpml/sgui";
import * as dkjson from "dkjson";

declare global {
    const system: DU.System;
    const library: DU.Library;
    const unit: DU.ControlUnit;
    const json: typeof dkjson;
    const vec3: typeof Vec3;
    const utils: typeof Utils;
    export const constants: typeof Constants;
    const pid: typeof PID;
    const sgui: SGui;

    /**
     * @noSelf
     */
    function getRoll(gravityDirection: Vector, forward: Vector, right: Vector): Vector;
    /**
     * returns Vector, rotation angle in degrees
     * @noSelf
     * @tupleReturn
     */
    function getAxisAngleRad(oldDir: Vector, newDir: Vector, preferredAxis: Vector): [Vector, number];
    interface DBPlayer {
        id: number;
        name: string;
        worldPos: Vector;
    }
    // interface DBConstruct {
    //     id: number;
    //     owner: number;
    //     type: DU.Radar.ConstructType;
    //     size: number;
    //     worldPos: Vector;
    //     worldSpeed: Vector;
    //     worldAcceleration: Vector;
    //     pos: Vector;
    //     speed: Vector;
    //     acceleration: Vector;
    //     name: string;
    // }
    interface DBElement {
        id: number;
        name: string;
        hitPoints: number;
        maxHitPoints: number;
        mass: number;
        type: DU.ElementClass;
    }
    /**
     * @noSelf
     */
    interface Database {
        getPlayer(id: number): DBPlayer;
        // getConstruct(radar: DU.Radar, id: number): DBConstruct;
        getElement(core: DU.Core, id: number): DBElement;
    }
    const database: Database;
    enum controlMasterModeId {
        travel,
        cruise
    }
    enum axisCommandId {
        longitudinal = 0,
        lateral = 1,
        vertical = 2
    }
    enum axisCommandType {
        byThrottle,
        byTargetSpeed,
        unused
    }
    interface AxisCommand {
        control: DU.ControlUnit;
        system: DU.System;
        core: DU.Core;
        mass: number;
        commandAxis: axisCommandId;
        commandType: axisCommandType;
        actionRepeatDelay: number;
        actionTriggeredTime: number;
        mouseBackToNeutralDuration: number;
        mouseBackToNeutralCountdown: number;
        updateSequenceStartValue: number;
        throttle: number;
        throttleAtomicStepValue: number;
        throttleMouseStepScale: number;
        targetSpeed: number;
        targetSpeedPID: typeof PID;
        customTargetSpeedRanges: number[];
        targetSpeedRanges: number[];
        targetSpeedMaxRange: number;
        targetSpeedAtomicStepValue: number;
        lastCurrentSpeed: number;
        new: (this: void, commandAxis: axisCommandId, control: DU.ControlUnit, core: DU.Core, system: DU.System) => AxisCommand;
        setupCustomTargetSpeedRanges(customTargetSpeedRanges: number[]): void;
        getTargetSpeedCurrentStep(): number;
        computeTargetSpeedStepValue(referenceSpeed: number, commandStep: number): number;
        onMasterModeChanged(masterModeId: controlMasterModeId): void;
        resetCommand(): void;
        updateCommandFromMouseWheel(mouseWheelInc: number): void;
        updateCommandFromActionStart(commandStep: number): void;
        updateCommandFromActionStop(commandStep: number): void;
        updateCommandFromActionLoop(commandStep: number): void;
        getCommandValue(): number;
        updateCommandByStep(commandStep: number): void;
        setCommandByThrottle(throttle: number): void;
        setCommandByTargetSpeed(targetSpeed: number): void;
        getAccelerationCommandToTargetSpeed(currentAxisSpeedMS: number): number;
        composeAxisAccelerationFromThrottle(tags: string): Vector;
        composeAxisAccelerationFromTargetSpeed(): Vector;
    }
    const AxisCommand: AxisCommand;
    interface AxisCommandManager {
        masterModeId: controlMasterModeId;
        control: DU.ControlUnit;
        system: DU.System;
        axisCommands: {
            [axisCommandId.longitudinal]: AxisCommand;
            [axisCommandId.lateral]: AxisCommand;
            [axisCommandId.vertical]: AxisCommand;
        };
        targetGroundAltitudeTriggeredTime: number;
        targetGroundAltitutdeActivated: boolean;
        targetGroundAltitude: number;
        targetGroundAltitudeCapabilities: any;
        new: (this: void, system: DU.System, control: DU.ControlUnit, core: DU.Core) => AxisCommandManager;
        getMasterMode(): controlMasterModeId;
        getAxisCommandType(commandAxis: axisCommandId): AxisCommand;
        setMasterMode(masterMode: controlMasterModeId): void;
        updateCommandFromMouseWheel(commandAxis: axisCommandId, mouseWheelInc: number): void;
        resetCommand(commandAxis: axisCommandId): void;
        updateCommandFromActionStart(commandAxis: axisCommandId, commandStep: number): void;
        updateCommandFromActionStop(commandAxis: axisCommandId, commandStep: number): void;
        updateCommandFromActionLoop(commandAxis: axisCommandId, commandStep: number): void;
        getThrottleCommand(commandAxis: axisCommandId): number;
        getTargetSpeed(commandAxis: axisCommandId): number;
        setupCustomTargetSpeedRanges(commandAxis: axisCommandId, customTargetSpeedRanges: number[]): void;
        getTargetSpeedCurrentStep(commandAxis: axisCommandId): number;
        getCurrentToTargetDeltaSpeed(commandAxis: axisCommandId): number;
        setThrottleCommand(commandAxis: axisCommandId, throttle: number): void;
        setTargetSpeedCommand(commandAxis: axisCommandId, targetSpeed: number): void;
        composeAxisAccelerationFromThrottle(tags: string, commandAxis: axisCommandId): Vector;
        composeAxisAccelerationFromTargetSpeed(commandAxis: axisCommandId): Vector;
        activateGroundEngineAltitudeStabilization(): void;
        setTargetGroundAltitude(targetAltitude: number): void;
        updateTargetGroundAltitudeFromActionStart(altitudeStabilizationInc: number): void;
        updateTargetGroundAltitudeFromActionLoop(altitudeStabilizationInc: number): void;
        updateGroundEngineAltitudeStabilization(): void;
    }
    const AxisCommandManager: AxisCommandManager;
    interface NavigatorOrientation {
        up: Vector;
        forward: Vector;
        right: Vector;
        worldUp: Vector;
        worldForward: Vector;
        worldRight: Vector;
    }
    interface Navigator {
        system: DU.System;
        core: DU.Core;
        control: DU.ControlUnit;
        boosterState: number;
        boosterStateHasChanged: boolean;
        mass: number;
        imass: number;
        orient: NavigatorOrientation;
        axisCommandManager: AxisCommandManager;
        new: (this: void, system: DU.System, core: DU.Core, control: DU.ControlUnit) => Navigator;
        toggleBoosters(): void;
        setBoosterCommand(tags: string): void;
        update(): void;
        maxForceForward(): number;
        maxForceBackward(): number;
        isCruiseControl(): boolean;
        isTravelMode(): boolean;
        getTargetGroundAltitude(): number;
        setEngineCommand(taglist: string, acceleration: Vector, angularAcceleration: Vector, keepForceCollinearity: boolean, keepTorqueCollinearity: boolean, priority1SubTag: string, priority2SubTag: string, priority3SubTag: string, toleranceRatioToSkipOtherPriorities: number): void;
        setEngineForceCommand(taglist: string, acceleration: Vector, keepForceCollinearity: boolean, priority1SubTag: string, priority2SubTag: string, priority3SubTag: string, toleranceRatioToSkipOtherPriorities: number): void;
        setEngineTorqueCommand(taglist: string, angularAcceleration: Vector, keepTorqueCollinearity: boolean, priority1SubTag: string, priority2SubTag: string, priority3SubTag: string, toleranceRatioToSkippOtherPriorities: number): void;
    }
    const Navigator: Navigator;
}
export {};
