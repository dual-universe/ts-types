# Changelog
## v1.5.0 2020-11-8
#### added:
- ControlUnit: stabilization functions
- Core: `getElementPositionById`, `getElementTagsById`
- Radar


##v1.4.0 2020-9-18
#### added:
- missing widget types and their data shape
- Weapon

#### fixed:
- updated all the Data interfaces

## v1.3.0 - 2020-9-5
#### added:
- License file
- `Radar.Data` json shape

## v1.2.0 - 2020-9-3
#### added:
- `json` and `sgui` as globals.

## v1.1.0 - 2020-8-31
#### added:
- missing `radar`, and `periscope` variants for `WidgetType`

## v1.0.0 - 2020-8-31
Update for beta launch. 
#### added:
- documentation for all the functions.
- `groudaltitudeup`,`groundaltitudedown`, and `warp` actionkeys
- `AntiGravityGenerator.getBaseAltitude()`
- `Industry.getStatus()`
- new Navigator functions
- Firework

#### removed:
- deprecated functions: `Core.getElementList`
- Radar API excluding the base Element functions

## v0.3.0 - 2020-06-07
#### added:
- jsdocs for `@event` to DU elements.
- events for system/unit.

## v0.2.0 - 2020-04-26
#### added:
- missing functions to unit:

        isMouseDirectControlActivated(): Bool;
        isMouseVirtualJoystickActivated(): Bool;
        isAnyHeadlightSwitchedOn(): Bool;
        isRemoteControlled(): Bool;

#### fixes:
- changed return type for `unit.isMouseControlActivated()` to `Bool`


## v0.1.0 - 2020-04-06
#### added:
- added dot notation modules

#### fixes:
- fixed @noSelf annotation in cpml/utils and pl/types
