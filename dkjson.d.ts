/**
 * @noResolution
 */
declare module "dkjson" {
    interface EncodeState {
        indent?: boolean;
        keyorder?: (string | number)[];
        level?: number;
        buffer?: string[];
        bufferlen?: number;
        exception?: (this: void, reason?: string, value?: any, state?: this, defaultmessage?: string) => void;
    }
    /**
     * @noSelf
     */
    interface Dkjson {
        encode(table: Object, state?: EncodeState): string;
        decode<TReturn = {}>(json: string, position?: number, nullvalue?: any): TReturn;
        quotestring(str: string): string;
        null: object;
    }
    const dkjson: Dkjson;
    export = dkjson;
}
