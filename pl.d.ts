/**
 * @noResolution
 */
declare module "pl.types" {
    import * as _ from "pl/types";
    export = _;
}
/**
* @noResolution
* @noSelf
 */
declare module "pl/types" {
    export function is_callable(obj: any): boolean;
    type LuaType = "nil" | "boolean" | "number" | "string" | "function" | "userdata" | "thread" | "table";
    export function type(obj: any): LuaType;
    export function is_integer(x: any): boolean;
    export function is_empty(o: any, ignore_spaces?: boolean): boolean;
    export function is_indexable(val: any): boolean;
    export function is_iterable(val: any): boolean;
    export function is_writeable(val: any): boolean;
    export function to_bool(o: any, true_strs?: string[], check_objs?: boolean): boolean;
}

// TODO the rest of pl...