/**
 * @noSelfInFile
 */
/**
 * Structural types aliases.
 * Useful only for viewing definitions.
 */
declare type Seconds = number;
/**
 * Number between 0 and 1
 */
declare type Percent = number;
declare type Radians = number;
declare type Degrees = number;
declare type Json = string;
declare type EventFilter = void;
declare type Vec3Tuple = [ number, number, number ];
declare type Vec2Tuple = [ number, number ];

/**
 * @noSelf
 */
interface Toggleable {
    activate(): void;

    deactivate(): void;

    toggle(): void;
}

/**
 * @noSelf
 */
interface Stateful<T> {
    getState(): T;
}

/**
 * @noSelf
 */
interface Pressable {
    /**
     * @deprecated This is an event filter and is not available on the object
     * @event
     */
    pressed(): EventFilter;

    /**
     * @deprecated This is an event filter and is not available on the object
     * @event
     */
    released(): EventFilter;
}

interface Enterable {
    /**
     * @deprecated This is an event filter and is not available on the object
     * @event
     */
    enter(id: number): EventFilter;

    /**
     * @deprecated This is an event filter and is not available on the object
     * @event
     */
    leave(id: number): EventFilter;
}

/**
 * @noSelf
 * TODO getElementClassName generics
 */
interface Element<WType extends DU.WidgetType = DU.WidgetType.None> {
    /**
     * Show the element widget in the in-game widget stack
     */
    show(): void;

    /**
     * Hide the element widget in the in-game widget stack
     */
    hide(): void;

    /**
     * Get element data as json string
     */
    getData(): Json;

    /**
     * Get element data id
     */
    getDataId(): number;

    /**
     * get widget type compatible with the element data
     */
    getWidgetType(): DU.WidgetType;

    /**
     * the element integrity between 0 and 100
     */
    getIntegrity(): number;

    /**
     * The element current hitpoints (0 = destroyed)
     */
    getHitPoints(): number;

    /**
     * The elements maximum hit points when it's fully functional
     */
    getMaxHitPoints(): number;

    /**
     * A construct unique ID for the element
     */
    getId(): number;

    /**
     * The mass of the element
     */
    getMass(): number;

    /**
     * The class of the element
     */
    getElementClass(): DU.ElementClass;

    /**
     * @deprecated This is an event filter and is not available on the object
     * @event
     */
    start(): EventFilter;

    /**
     * @deprecated This is an event filter and is not available on the object
     * @event
     */
    stop(): EventFilter;
}

declare namespace DU {
    const enum Bool {
        False = 0,
        True = 1
    }

    const enum Outcome {
        Failure = 0,
        Success = 1
    }

    const enum Action {
        Forward = "forward",
        Backward = "backward",
        YawLeft = "yawleft",
        YawRight = "yawright",
        StrafeLeft = "strafeleft",
        StrafeRight = "straferight",
        Left = "left",
        Right = "right",
        Up = "up",
        Down = "down",
        GroundAltUp = "groundaltitudeup",
        GroundAltDown = "groundaltitudedown",
        LAlt = "lalt",
        LShift = "lshift",
        Gear = "gear",
        Light = "light",
        Brake = "brake",
        Opt1 = "option1",
        Opt2 = "option2",
        Opt3 = "option3",
        Opt4 = "option4",
        Opt5 = "option5",
        Opt6 = "option6",
        Opt7 = "option7",
        Opt8 = "option8",
        Opt9 = "option9",
        StopEngines = "stopengines",
        SpeedUp = "speedup",
        SpeedDown = "speeddown",
        AntiGravity = "antigravity",
        Booster = "booster",
        Warp = "warp"
    }

    // TODO I assume "warpdrive" is a widget type as well, but don't have one yet to test
    const enum WidgetType {
        None = "",
        Engine = "engine_unit",
        FuelTank = "fuel_container",
        ControlUnit = "basic_control_unit",
        Cockpit = "cockpit",
        Core = "core",
        Agg = "antigravity_generator",
        Gyro = "gyro",
        Weapon = "weapon",
        Radar = "radar",
        Periscope = "periscope",
        Text = "text",
        Title = "title",
        Value = "value",
        Gauge = "gauge"
    }

    namespace Widget {
        export type Engine = Omit<Engine.Data, "name" | "helperId"|"type">

        export type FuelTank = Omit<Container.Data, "name" | "helperId"|"type">

        export interface ControlUnit {
            // nothing to see here
        }

        // TODO `controlData` object doesn't work. The widget type is fixed with cruise control/travel mode
        export type Cockpit = Omit<ControlUnit.Data, "name" | "helperId"|"type"|"controlData">

        export type Core = Omit<Core.Data, "name" | "helperId"|"type">

        export interface Agg {
            // TODO Widget Kind Shape (Agg)
        }

        export type Gyro = Omit<Gyro.Data, "name" | "helperId" |"type">

        export interface Weapon {
            // TODO Widget Kind Shape (Weapon)
        }

        export type Radar = Omit<Radar.Data, "name" | "helperId" | "type">

        export interface Periscope {
            // I'm not able to mock this without using getDataId() from a radar.
            // encode(decode(getData()) leaves a blank widget. There must be hidden data.
        }

        export interface Text {
            text: string;
        }

        export interface Title {
            text: string;
        }

        export interface Gauge {
            percentage: number;
        }

        export interface Value {
            label: string;
            value: string;
            unit: string;
        }
    }

    const enum ElementClass {
        ProgrammingBoard = "Generic",
        HovercraftSeat = "CockpitHovercraftUnit",
        CommandSeat = "CockpitCommandmentUnit",
        Cockpit = "CockpitFighterUnit",
        GunnerModule = "PVPSeatUnit",
        EmergencyController = "ECU",
        RemoteControl = "RemoteControlUnit",
        DynamicCore = "CoreUnitDynamic",
        StaticCore = "CoreUnitStatic",
        SpaceCore = "CoreUnitSpace",
        Agg = "AntiGravityGeneratorUnit",
        AtmoEngine = "AtmosphericEngine",
        SpaceEngine = "SpaceEngine",
        RocketEngine = "RocketEngine",
        HoverEngine = "Hovercraft",
        VerticalBooster = "VerticalBooster",
        Airbrake = "Airbrake",
        Spacebrake = "Spacebrake",
        Adjustor = "Adjustor",
        Aileron = "Aileron2",
        Wing = "Wing2",
        Stabilizer = "Stabilizer",
        AtmoFuelTank = "AtmoFuelContainer",
        SpaceFuelTank = "SpaceFuelContainer",
        RocketFuelTank = "RocketFuelContainer",
        Container = "ItemContainer",
        ForceField = "ForceFieldUnit",
        LandingGear = "LandingGearUnit",
        Door = "DoorUnit",
        Light = "LightUnit",
        DetectionZone = "DetectionZoneUnit",
        Button = "ManualButtonUnit",
        PressureTile = "PressureTileUnit",
        Industry = "IndustryUnit",
        Counter = "CounterUnit",
        Gyro = "GyroUnit",
        LaserEmitter = "LaserEmitterUnit",
        LaserReceiver = "LaserDetectorUnit",
        Emitter = "EmitterUnit",
        Receiver = "ReceiverUnit",
        Databank = "DataBankUnit",
        Radar = "RadarUnit",
        PvpRadar = "RadarPvPSpace",
        Screen = "ScreenUnit",
        Sign = "ScreenSignUnit",
        Switch = "ManualSwitchUnit",
        Telemeter = "TelemeterUnit",
        MissileXS = "WeaponMissileExtraSmall",
        MissileS = "WeaponMissileSmall",
        MissileM = "WeaponMissileMedium",
        MissileL = "WeaponMissileLarge",
        RailgunXS = "WeaponRailgunExtraSmall",
        RailgunS = "WeaponRailgunSmall",
        RailgunM = "WeaponRailgunMedium",
        RailgunL = "WeaponRailgunLarge",
        LaserXS = "WeaponLaserExtraSmall",
        LaserS = "WeaponLaserSmall",
        LaserM = "WeaponLaserMedium",
        LaserL = "WeaponLaserLarge",
        CannonXS = "WeaponCannonExtraSmall",
        CannonS = "WeaponCannonSmall",
        CannonM = "WeaponCannonMedium",
        CannonL = "WeaponCannonLarge"
    }

    const enum ConstructSize {
        Xs = "XS",
        S = "S",
        M = "M",
        L = "L"
    }

    /**
     * Anti Gravity
     */
    namespace AntiGravityGenerator {
        const enum AntiGravityGeneratorState {
            Inactive = 0,
            Active = 1
        }

        interface Data {
            antiGPower: number;
            antiGravityField: number;
            baseAltitude: number;
            helperId: WidgetType.Agg;
            name: string;
            showError: boolean;
            type: WidgetType.Agg;
        }
    }

    /**
     * @noSelf
     */
    interface AntiGravityGenerator extends Element<DU.WidgetType.Agg>, Toggleable {
        getState(): AntiGravityGenerator.AntiGravityGeneratorState;

        setBaseAltitude(altitude: number): void;

        getBaseAltitude(): number;
    }

    /**
     * Button
     */
    namespace Button {
        const enum ButtonState {
            Released = 0,
            Pressed = 1
        }
    }

    /**
     * @noSelf
     */
    interface Button extends Element, Stateful<Button.ButtonState>, Pressable {
    }

    namespace Container {
        /**
         * This only applies to Fuel tanks
         * TODO I'm assuming "fuel_container_rocket_fuel"
         */
        interface Data {
            "helperId": "fuel_container_space_fuel" | "fuel_container_atmo_fuel" | "fuel_container_rocket_fuel";
            "name": string;
            "percentage": number;
            "timeLeft": string;
            "type": WidgetType.FuelTank;
        }
    }

    /**
     * Container
     *
     * @noSelf
     * TODO separate FuelTank vs Container to make the widgets easier?
     */
    interface Container extends Element<DU.WidgetType.FuelTank> {
        /**
         * return the container content mass (the sum of the mass of all the item it contains)
         */
        getItemsMass(): number;

        /**
         * Returns the container self mass, as if it were empty
         */
        getSelfMass(): number;
    }

    /**
     * Control Unit
     */
    namespace ControlUnit {
        const enum Axis {
            Longitudinal = 0,
            Lateral = 1,
            Vertical = 2
        }

        const enum CommandType {
            Throttle = 0,
            Speed = 1,
            Hidden = 2
        }

        const enum MasterMode {
            Travel = 0,
            Cruise = 1
        }

        interface Data {
            acceleration: number;
            airDensity: number;
            airResistance: number;
            atmoThrust: number;
            controlData: {
                axisData: {
                    commandValue: number;
                    commandType: number;
                    speed: number;
                }[];
                currentMasterMode: MasterMode;
                masterModeData: {
                    name: string;
                }[];
            };
            controlMasterModeId: MasterMode;
            currentBrake: number;
            elementId: string;
            helperId: WidgetType.ControlUnit | WidgetType.Cockpit;
            maxBrake: number;
            name: string;
            showHasBrokenFuelTank: boolean;
            showOutOfFuel: boolean;
            showOverload: boolean;
            showScriptError: boolean;
            showSlowDown: boolean;
            spaceThrust: number;
            speed: number;
            type: WidgetType.ControlUnit | WidgetType.Cockpit;
        }
    }

    /**
     * @noSelf
     */
    interface ControlUnit extends Element<DU.WidgetType.Cockpit | DU.WidgetType.ControlUnit> {
        /**
         * Stops the control unit and exits
         */
        exit(): void;

        /**
         * Set up a timer with a given tag ID in a given period. This will start to trigger the 'tick' event with the
         * corresponding ID as an argument, to help you identify what is ticking, and when.
         */
        setTimer(timerTagId: string, period: Seconds): void;

        /**
         * Stop the timer with the given ID
         */
        stopTimer(timerTagId: string): void;

        /**
         * Returns the local atmpshere density, between 0 and 1
         */
        getAtmosphereDensity(): Percent;

        /**
         * Returns the closest planet influence, between 0 and 1.
         */
        getClosestPlanetInfluence(): number;

        /**
         * Return the relative position (in world coordinates) of the player currently running the control unit.
         */
        getMasterPlayerRelativePosition(): Vec3Tuple;

        /**
         * Return the ID of the player currently running the control unit.
         */
        getMasterPlayerId(): number;

        /**
         * Automatically assign the engines within the taglist to result in the given acceleration and
         * angular acceleration provided. Can only be called within the system.flush event. If engines
         * designated by the tags are not capable of producing the desired command, setEngineCommand will
         * try to do its best to approximate it.
         */
        setEngineCommand(taglist: string, acceleration: Vec3Tuple, angularAcceleration: Vec3Tuple, keepForceCollinearity: boolean, keepTorqueCollinearity: boolean, priority1SubTag: string, priority2SubTag: string, priority3SubTag: string, toleranceRatioToSkippOtherPriorities: number): void;

        /**
         * Force the thrust value for all the engines within the tag list
         */
        setEngineThrust(taglist: string, thrust: number): void;

        /**
         * Set the value of throttle in the cockpit, which will be displayed in the cockpit widget when flying
         */
        setAxisCommandValue(axis: ControlUnit.Axis, commandValue: number): void;

        /**
         * Get the value of throttle in the cockpit in the range (-1,1)
         */
        getAxisCommandValue(axis: ControlUnit.Axis): number;

        /**
         * Set the properties of an axis command. These properties will be used to display the command in UI.
         */
        setupAxisCommandProperties(axis: ControlUnit.Axis, commandType: ControlUnit.CommandType): void;

        /**
         * Get the current master mode in use. The mode is set by clicking the UI button
         * or using the associated keybinding.
         */
        getControlMasterModeId(): ControlUnit.MasterMode;

        /**
         * Cancel the current master mode in use.
         */
        cancelCurrentControlMasterMode(): void;

        /**
         * Check landing gear status
         */
        isAnyLandingGearExtended(): Bool;

        /**
         * Extend landing gears
         */
        extendLandingGears(): void;

        /**
         * Retract landing gears
         */
        retractLandingGears(): void;

        /**
         * Check if a mouse control scheme is selected
         */
        isMouseControlActivated(): Bool;

        /**
         * Check if the mouse control direct scheme is selected
         */
        isMouseDirectControlActivated(): Bool;

        /**
         * Check if the mouse control virtual joystick scheme is selected
         */
        isMouseVirtualJoystickActivated(): Bool;

        /**
         * Check lights status
         */
        isAnyHeadlightSwitchedOn(): Bool;

        /**
         * Switch on lights
         */
        switchOnHeadlights(): void;

        /**
         * Switch off lights
         */
        switchOffHeadlights(): void;

        /**
         * Check if the construct is remote controlled
         */
        isRemoteControlled(): Bool;

        /**
         * The ground engines will stabilize to this altitude within their limits. The stabilization will
         * be done by adjusting thrust to never go over the target altitude. This includes VerticalBooster
         * and HoverEngine
         */
        activateGroundEngineAltitudeStabilization(targetAltitude: number): void;

        /**
         * Returns the ground engines stabilization altitude
         */
        getSurfaceEngineAltitudeStabilization(): number;

        /**
         * The ground engines will behave like regular engines. This includes verticalBooster and HoverEngine
         */
        deactivateGroundEngineAltitudeStabilization(): void;

        /**
         * Returns the ground engine stabilization altitude capabilities (lower and upper ranges)
         * Return value is the stabilization altitude capabilities for the least powerful engine and the most powerful engine
         */
        computeGroundEngineAltitudeStabilizationCapabilities(): [number, number];

        /**
         * Return the current throttle value (range: -100 and 100)
         */
        getThrottle(): number;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        tick(timerId: string): EventFilter;
    }

    /**
     * Core
     */
    namespace Core {
        const enum StickerId {
            Zero = 0,
            One = 1,
            Two = 2,
            Three = 3,
            Four = 4,
            Five = 5,
            Six = 6,
            Seven = 7,
            Eight = 8,
            Nine = 9,
            Invalid = -1
        }

        const enum NumberOrientation {
            Front = "front",
            Side = "side"
        }

        const enum ArrowOrientation {
            Up = "up",
            Down = "down",
            North = "north",
            South = "south",
            East = "east",
            West = "west"
        }

        interface Data {
            altitude: number;
            gravity: number;
            helperId: WidgetType.Core;
            name: string;
            type: WidgetType.Core;
        }
    }

    /**
     * @noSelf
     */
    interface Core extends Element<DU.WidgetType.Core> {
        /**
         * Returns the mass of the construct in kilograms
         */
        getConstructMass(): number;

        /**
         * Returns the inertial mass of the construct, calculated as 1/3 of the trace of the inertial tensor
         */
        getConstructIMass(): number;

        /**
         * Returns the constructs cross sectional surface in the current direction of movement
         */
        getConstructCrossSection(): number;

        /**
         * Returns the construct max kinematics parameters in both atmo and space range, in newtons.
         * Kinematics parameters designate here the maximal positive and negative base force that the
         * construct is capable of producing along the chosen axis vector, as defined by the core unit or gyro.
         * In practice, this gives you an estimate of the maximum thrust your ship is capable of producing in
         * space or in atmosphere, as well as the max reverse thrust. These are theoretical estimates and
         * correspond with the addition of the maxThrustBase along the corresponding axis. It might not reflect
         * the accurate current max thrust capacity of yours hip, which depends on various local
         * conditions (atmospheric density, orientation, obstruction, engine damage, etc). This is typically used in
         * conjunction with the control unit throttle to setup the desired forward acceleration.
         */
        getMaxKinematicsParametersAlongAxis(taglist: string, CRefAxis: Vec3Tuple): [ number, number, number, number ];

        /**
         * Returns the world position of the construct
         */
        getConstructWorldPos(): Vec3Tuple;

        /**
         * Returns the construct unique ID
         */
        getConstructId(): number;

        /**
         * Returns the acceleration torque generated by air resistance.
         */
        getWorldAirFrictionAngularAcceleration(): Vec3Tuple;

        /**
         * Returns the acceleration generated by air resistance
         */
        getWorldAirFrictionAcceleration(): Vec3Tuple;

        /**
         * Spawns a number sticker in the 3D world, with coordinates relative to the construct.
         */
        spawnNumberSticker(nb: number, x: number, y: number, z: number, orientation: Core.NumberOrientation): Core.StickerId;

        /**
         * Spawns an arrow sticker in the 3D world, with coordinates relative to the construct.
         */
        spawnArrowSticker(x: number, y: number, z: number, orientation: Core.ArrowOrientation): Core.StickerId;

        /**
         * Delete the referenced sticker.
         */
        deleteSticker(index: Core.StickerId): Outcome;

        /**
         * Move the referenced sticker.
         */
        moveSticker(index: Core.StickerId, x: number, y: number, z: number): Outcome;

        /**
         * Rotate the referenced sticker.
         */
        rotateSticker(index: Core.StickerId, angleX: number, angleY: number, angleZ: number): Outcome;

        /**
         * List of all the UIDs of the elements of this construct
         */
        getElementIdList(): number[];

        /**
         * Name of the element, identified by its UID
         */
        getElementNameById(uid: number): string;

        /**
         * Type of the element, identified by its UID
         */
        getElementTypeById(uid: number): string;

        /**
         * Current level of hit points of the element
         */
        getElementHitPointsById(uid: number): number;

        /**
         * Max level of hit points of the element
         */
        getElementMaxHitPointsById(uid: number): number;

        /**
         * Mass of the element
         */
        getElementMassById(uid: number): number;

        /**
         * Rotation of the element. Returns a quaternion: (x,y,z,w)
         */
        getElementPositionById(uid: number): [number, number, number, number];

        /**
         * List of tags associated to the element. Returns tags as a JSON list.
         */
        getElementTagsById(uid: number): string;

        /**
         * Altitude above sea level, with respect to the closest planet (0 in space)
         */
        getAltitude(): number;

        /**
         * local gravity intensity
         */
        g(): number;

        /**
         * local gravity vector in world coordinates
         */
        getWorldGravity(): Vec3Tuple;

        /**
         * Vertical unit vector along gravity, in world coordinates (0 in space)
         */
        getWorldVertical(): Vec3Tuple;

        /**
         * The constructs angular velocity, in world coordinates
         */
        getAngularVelocity(): Vec3Tuple;

        /**
         * The constructs angular acceleration, in construct local coordinates
         */
        getWorldAngularVelocity(): Vec3Tuple;

        /**
         * The constructs angular acceleration, in construct local coordinates
         */
        getAngularAcceleration(): Vec3Tuple;

        /**
         * The constructs angular acceleration, in world coordinates
         */
        getWorldAngularAcceleration(): Vec3Tuple;

        /**
         * The constructs linear velocity, in construct local coordinates
         */
        getVelocity(): Vec3Tuple;

        /**
         * The constructs linear velocity, in world coordinates
         */
        getWorldVelocity(): Vec3Tuple;

        /**
         * The constructs linear acceleration, in world coordinates
         */
        getWorldAcceleration(): Vec3Tuple;

        /**
         * The constructs linear acceleration, in construct local coordinates
         */
        getAcceleration(): Vec3Tuple;

        /**
         * The constucts current orientation up vector, in construct local coordinates
         */
        getConstructOrientationUp(): Vec3Tuple;

        /**
         * The constructs current orientation right vector, in construct local coordinates
         */
        getConstructOrientationRight(): Vec3Tuple;

        /**
         * The constructs current orientation forward vector, in construct local coordinates
         */
        getConstructOrientationForward(): Vec3Tuple;

        /**
         * The constructs current orientation up vector, in world coordinates
         */
        getConstructWorldOrientationUp(): Vec3Tuple;

        /**
         * The constructs current orientation right vector, in world coordinates
         */
        getConstructWorldOrientationRight(): Vec3Tuple;

        /**
         * The constructs current orientation forward vector, in world coordinates
         */
        getConstructWorldOrientationForward(): Vec3Tuple;
    }

    /**
     * Counter
     */
    namespace Counter {}
    /**
     * @noSelf
     */
    interface Counter extends Element {
        /**
         * Returns the rank of the currently active OUT plug.
         */
        getCounterState(): number;

        /**
         * Moves the counter one step further
         */
        next(): void;
    }

    /**
     * Databank
     */
    namespace Databank {}
    /**
     * @noSelf
     */
    interface Databank<T extends object = any> extends Element {
        /**
         * Clear the data bank
         */
        clear(): void;

        /**
         * Returns the number of keys that are stored inside the data bank
         */
        getNbKeys(): number;

        /**
         * Returns all the keys in the data bank
         */
        getKeys(): (keyof T)[];

        /**
         * Returns 1 if the key is present in the databank, 0 otherwise
         */
        hasKey(key: string): Bool;

        /**
         * Stores a string value at the given key
         */
        setStringValue<K extends keyof T>(key: K, val: T[K]): void;

        /**
         * Returns value stored in the given key as a string
         */
        getStringValue<K extends keyof T>(key: K): T[K] extends string ? T[K] : never;

        /**
         * Stores an integer value at the given key
         */
        setIntValue<K extends keyof T>(key: K, val: number): void;

        /**
         * Returns value stored in te given key as an integer
         */
        getIntValue<K extends keyof T>(key: K): T[K] extends number ? T[K] : never;

        /**
         * Stores a floating number value at the given key
         */
        setFloatValue<K extends keyof T>(key: K, val: number): void;

        /**
         * Returns value stored in the given key as a floating number
         */
        getFloatValue<K extends keyof T>(key: K): T[K] extends number ? T[K] : never;
    }

    /**
     * Detection Zone
     */
    namespace DetectionZone {}
    /**
     * @noSelf
     */
    interface DetectionZone extends Element, Enterable {
    }

    /**
     * Door
     */
    namespace Door {
        const enum DoorState {
            Closed = 0,
            Open = 1
        }
    }

    /**
     * @noSelf
     */
    interface Door extends Element, Toggleable, Stateful<Door.DoorState> {
    }

    /**
     * Emitter
     */
    namespace Emitter {}
    /**
     * @noSelf
     */
    interface Emitter extends Element {
        /**
         * Send a message on the given channel
         */
        send(channel: string, message: string): void;

        /**
         * Returns the emitter range
         */
        getRange(): number;
    }

    /**
     * Engine
     */
    namespace Engine {
        const enum EngineState {
            Inactive = 0,
            Active = 1
        }

        interface Data {
            currentMaxThrust: number;
            currentThrust: number;
            helperId: WidgetType.Engine;
            maxThrustBase: number;
            name: string;
            type: WidgetType.Engine;
        }
    }

    /**
     * @noSelf
     */
    interface Engine extends Element<DU.WidgetType.Engine>, Toggleable, Stateful<Engine.EngineState> {
        /**
         * Set the engine thrust between 0 and max thrust
         */
        setThrust(thrust: number): void;

        /**
         * Returns the maximal thrust the engine can deliver in principle, under optimal conditions.
         * Note that the actual max thrust will most of th etime be less than maxThrustBase
         */
        getMaxThrustBase(): number;

        /**
         * Returns the maximal thrust the engine can deliver at the moment, which might depend on various
         * conditions like atmospheric density, obstruction, orientation, etc. The actual thrust will be anything
         * below this maxThrust, which defines the current max capability of the engine.
         */
        getCurrentMaxThrust(): number;

        /**
         * Returns the minimal thrust the engine can deliver at the moment (can be more than zero),
         * which will depend on various conditions like atmospheric density, obstruction, orientation, etc.
         * Most of the time, this will be 0 but it can be greater than 0, particularly for ailerons, in which case
         * the actual thrust will be at least equal to minThrust.
         */
        getCurrentMinThrust(): number;

        /**
         * Returns the ratio between the current MaxThrust and the base MaxThrust
         */
        getMaxThrustEfficiency(): number;

        /**
         * Returns the current thrust level of the engine
         */
        getThrust(): number;

        /**
         * Returns the engine torque axis
         */
        torqueAxis(): Vec3Tuple;

        /**
         * Returns the engine thrust direction
         */
        thrustAxis(): Vec3Tuple;

        /**
         * @deprecated does not work. Use `distance()`
         */
        getDistanceToObstacle(): number;

        /**
         * Returns the engine thrust direction (getDistanceToObstacle is incorrect)
         */
        distance(): number;

        /**
         * is the engine out of fuel?
         */
        isOutOfFuel(): Bool;

        /**
         * Is the engine linked to a broken fuel tank?
         */
        hasBrokenFuelTank(): Bool;

        /**
         * The engine rate of fuel consumption per newton delivered per second
         */
        getCurrentFuelRate(): number;

        /**
         * Returns the ratio between the current fuel rate and the theoretical noninal fuel rate
         */
        getFuelRateEfficiency(): number;

        /**
         * The time needed for the engine to reach 50% of its maximal thrust
         */
        getT50(): Seconds;

        /**
         * If the engine exhaust is obstructed by some element or voxel material, it will stop working
         * or may work randomly in an instable way and you should probably fix your design
         */
        isObstructed(): Bool;

        /**
         * Returns the obstruction ratio of the engine exhaust by elements and voxels. The more obstructed
         * the engine is, the less properly it will work.
         */
        getObstructionFactor(): Percent;

        /**
         * Tags of the engine
         */
        getTags(): string;

        /**
         * Set the tags of the engine
         * @param tags
         */
        setTags(tags: string): void;

        /**
         * The current rate of fuel consumption in m^3/s
         */
        getFuelConsumption(): number;
    }

    namespace Firework {
        const enum Type {
            Ball = 0,
            PalmTree = 1,
            Ring = 2,
            Shower = 3
        }

        const enum Color {
            Blue = 0,
            Gold = 1,
            Green = 2,
            Purple = 3,
            Red = 4,
            Silver = 5
        }
    }

    interface Firework extends Element {
        /**
         * Fire the firework
         */
        activate(): void;

        /**
         * Set the delay before the launched fireworks explodes. Max = 5s
         */
        setExplosionDelay(time: Seconds): void;

        /**
         * Set the speed at which the firework will be launched in m/s (impacts its altitude, depending on local
         * gravity) Max = 200 m/s
         */
        setLaunchSpeed(speed: number): void;

        /**
         * Set the type of the launched firework
         */
        setType(type: Firework.Type): void;

        /**
         * Set the color of the launched firework
         */
        setColor(color: Firework.Color): void;
    }

    /**
     * Force Field
     */
    namespace ForceField {
        const enum ForceFieldState {
            Inactive = 0,
            Active = 1
        }
    }

    /**
     * @noSelf
     */
    interface ForceField extends Element, Toggleable, Stateful<ForceField.ForceFieldState> {
    }

    /**
     * Gyro
     */
    namespace Gyro {
        const enum GyroState {
            Inactive = 0,
            Active = 1
        }

        interface Data {
            helperId: WidgetType.Gyro;
            name: string;
            pitch: number;
            roll: number;
            type: WidgetType.Gyro;
        }
    }

    /**
     * @noSelf
     */
    interface Gyro extends Element<DU.WidgetType.Gyro>, Toggleable, Stateful<Gyro.GyroState> {
        /**
         * The up vector of the gyro unit in local coordinates
         */
        localUp(): Vec3Tuple;

        /**
         * The forward vector of the gyro unit in local coordinates
         */
        localForward(): Vec3Tuple;

        /**
         * The right vector of the gyro unit in local coordinates
         */
        localRight(): Vec3Tuple;

        /**
         * The up vector of the gyro unit in world coordinates
         */
        worldUp(): Vec3Tuple;

        /**
         * The forward vector of the gyro unit in world coordinates
         */
        worldForward(): Vec3Tuple;

        /**
         * The right vector of the gyro unit in world coordinates
         */
        worldRight(): Vec3Tuple;

        /**
         * The pitch value relative to the gyro orientation and local gravity.
         */
        getPitch(): Degrees;

        /**
         * The roll value relative to the gyro orientation and the local gravity.
         */
        getRoll(): Degrees;
    }

    /**
     * Industry
     */
    namespace Industry {
        const enum AllowIngredientLoss {
            Forbid = 0,
            Enable = 1
        }

        const enum IndustryStatus {
            Stopped = "STOPPED",
            Running = "RUNNING",
            MissingIngredient = "JAMMED_MISSING_INGREDIENT",
            OutputFull = "JAMMED_OUTPUT_FULL",
            NoOutputContainer = "JAMMED_NO_OUTPUT_CONTAINER"
        }
    }

    /**
     * @noSelf
     */
    interface Industry extends Element {
        /**
         * Start the production and it will run unless it is stopped or the input resources run out
         */
        start(): void;

        /**
         * Start maintaining the specified quantity. Resumes production when the quantity in the output
         * container is too low, and pauses production when it is equal or higher.
         */
        startAndMaintain(qty: number): void;

        /**
         * Start the production of numBatches and then stop.
         */
        batchStart(numBatches: number): void;

        /**
         * End the job and stop. Production keeps going until it is complete, then it switches to
         * "STOPPED" status. If the output container is full, then it switches to "JAMMED"
         */
        softStop(): void;

        /**
         * Stop production immediately. The resources are given back to the input container. If there is not enough
         * room in the input containers, production stoppage is skipped if alowingredientloss is set to 0,
         * or ingredients are lost if set to 1
         */
        hardStop(allowIngredientLoss: Industry.AllowIngredientLoss): void;

        /**
         * Get the status of the industry
         */
        getStatus(): Industry.IndustryStatus;

        /**
         * Get the count of completed cycles since the player started the unit
         */
        getCycleCountSinceStartup(): number;

        /**
         * Get the efficiency of the industry
         */
        getEfficiency(): Percent;

        /**
         * Get the time elapsed in seconds since the player started the unit for the latest time
         */
        getUptime(): Seconds;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        completed(): EventFilter;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        statusChanged(status: Industry.IndustryStatus): EventFilter;
    }

    /**
     * Landing Gear
     */
    namespace LandingGear {
        const enum LandingGearState {
            Retracted = 0,
            Extended = 1
        }
    }

    /**
     * @noSelf
     */
    interface LandingGear extends Element, Toggleable, Stateful<LandingGear.LandingGearState> {
    }

    /**
     * Laser Detector
     */
    namespace LaserDetector {
        const enum LaserDetectorState {
            Miss = 0,
            Hit = 1
        }
    }

    /**
     * Laser Receiver / Laser Detector
     * it's listed as 'Laser Detector' in the API, but the element names are 'receiver'
     *
     * @noSelf
     */
    interface LaserDetector extends Element, Stateful<LaserDetector.LaserDetectorState> {
        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        laserHit(): EventFilter;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        laserRelease(): EventFilter;
    }

    /**
     * Laser Emitter
     */
    namespace LaserEmitter {
        const enum LaserEmitterState {
            Inactive = 0,
            Active = 1
        }
    }

    /**
     * @noSelf
     */
    interface LaserEmitter extends Element, Toggleable, Stateful<LaserEmitter.LaserEmitterState> {
    }

    /**
     * Library
     */
    namespace Library {}
    /**
     * @noSelf
     */
    interface Library {

        /**
         * Solve the 3D linear system M*x = c0 where M is defined by its column vectors c1,c2,c3
         */
        systemResolution3(vecC1: Vec3Tuple, vecC2: Vec3Tuple, vecC3: Vec3Tuple, vecC0: Vec3Tuple): Vec3Tuple;

        /**
         * Solve the 2D linear system M*x = c0 where M is defined by its column vectors c1,c2
         */
        systemResolution2(vecC1: Vec3Tuple, vecC2: Vec3Tuple, vecC0: Vec3Tuple): Vec2Tuple;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        start(): EventFilter;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        stop(): EventFilter;
    }

    /**
     * Light
     */
    namespace Light {
        const enum LightState {
            Off = 0,
            On = 1
        }
    }

    /**
     * @noSelf
     */
    interface Light extends Element, Toggleable, Stateful<Light.LightState> {
    }

    /**
     * Pressure Tile
     */
    namespace PressureTile {
        const enum PressureTileState {
            Released = 0,
            Pressed = 1
        }
    }

    /**
     * @noSelf
     */
    interface PressureTile extends Element, Stateful<PressureTile.PressureTileState>, Pressable {
    }

    /**
     * Radar
     */
    namespace Radar {

        interface Data {
            constructsList: {
                constructId: string;
                distance: number;
                inIdentifyRange: boolean;
                info: {
                    // this is for atmo radar. untested for space.
                    // info is an empty object if a construct isn't identified
                    mass: number;
                    speed: number;
                    radialSpeed: number;
                    angularSpeed: number;
                    // These seem to be Bool ints
                    antiGravity: number;
                    rocketBoosters: number;
                    atmoEngines: number;
                    radars: number;
                    weapons: number;
                    spaceEngines: number;
                    constructType: ConstructType; // this looks to be 0 = static, 1 = dynamic, 2 = space

                };
                isIdentified: boolean;
                myThreatStateToTarget: number; // TODO I think this is int Bool?
                name: string;
                size: ConstructSize;
                targetThreatState: number; // TODO same as myThreatStateToTarget
            }[];
            elementId: string;
            helperId: WidgetType.Radar;
            name: string;
            properties: {
                broken: boolean;
                errorMessage: string;
                identifiedConstructs: []; // TODO
                radarStatus: number;      // TODO looks like 0/1
                selectedConstruct: string;
                worksInEnvironment: boolean;
            };
            staticProperties: {
                maxIdentifiedTargets: number;
                ranges: {
                    identify128m: number;
                    identify16m: number;
                    identify32m: number;
                    identify64m: number;
                    scan: number;
                };
                worksInAtmosphere: boolean;
                worksInSpace: boolean;
            }
            type: WidgetType.Radar;
        }

        const enum ConstructType {
            Static,
            Dynamic,
            Space   // TODO verify
        }
    }

    /**
     * @noSelf
     */
    interface Radar extends Element<DU.WidgetType.Radar>, Enterable {
        /**
         * Return the current scan range of the radar (meters)
         */
        getRange(): number;

        /**
         * Returns the list of construct IDs currently detected in the range
         */
        getEntries(): number[];

        /**
         * Returns whether the target has an active transponder with matching tags
         */
        hasMatchingTransponder(id: string): Bool;

        /**
         * Returns the player id of the owner of the given construct, if in range
         */
        getConstructOwner(id: number): number;

        /**
         * Return the size of the bounding box of the given construct, if in range
         */
        getConstructSize(id: number): Vec3Tuple;

        /**
         * Return the type of the given construct
         */
        getConstructType(id): "static" | "dynamic";

        /**
         * Return the world coordinates of the given construct, if in range
         */
        getConstructWorldPos(id: number): Vec3Tuple;

        /**
         * Return the world velocity of the given construct, if in range
         */
        getConstructWorldVelocity(id: number): Vec3Tuple;

        /**
         * Return the world acceleration of the given construct, if in range
         */
        getConstructWorldAcceleration(id: number): Vec3Tuple;

        /**
         * Return the radar local coordinates of the given construct, if in range
         */
        getConstructPos(id: number): Vec3Tuple;
        /**
         * Return the radar local velocity of the given construct, if in range
         */
        getConstructVelocity(id: number): Vec3Tuple;

        /**
         * Return the radar local acceleration of the given construct, if in range
         */
        getConstructAcceleration(id: number): Vec3Tuple;

        /**
         * Return the name of the given construct, if defined
         */
        getConstructName(id: number): string;
    }

    /**
     * Receiver
     */
    namespace Receiver {}
    /**
     * @noSelf
     */
    interface Receiver extends Element {
        /**
         * Returns the receiver range
         */
        getRange(): number;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        receive(channel: string, message: string): EventFilter;
    }

    /**
     * Screen
     */
    namespace Screen {
        const enum ContentState {
            Invisible = 0,
            Visible = 1
        }

        const enum MouseState {
            Up = 0,
            Down = 1
        }
    }

    /**
     * @noSelf
     */
    interface Screen extends Element {
        /**
         * Display the given text at a the given coordinates on the screen, and return an id to move it later
         */
        addText(x: number, y: number, fontSize: number, text: string): number;

        /**
         * Displays the given tet centered in the screen with a font to maximize its visibility
         * @param text
         */
        setCenteredText(text: string): void;

        /**
         * Set the whole screen HTML content.
         */
        setHTML(html: string): void;

        /**
         * Display the given HTML content at the given coordinates, and return an id to move it later
         */
        addContent(x: number, y: number, html: string): number;

        /**
         * Displays SVG code which overrides pre-existing content.
         */
        setSVG(svg: string): void;

        /**
         * Update the eleemnt with the given ID (returned by setContent) with a new HTML content
         */
        resetContent(id: number, html: string): void;

        /**
         * Delete the element with the given ID (returned by setContent)
         */
        deleteContent(id: string): void;

        /**
         * Update the visibility of the element with the given ID (returned by setContent)
         */
        showContent(id: number, state: Screen.ContentState): void;

        /**
         * Move the eleemnt with the given ID (returned by setContent) to a new position on the screen
         */
        moveContent(id: number, x: number, y: number): void;

        /**
         * Returns the x-coordinate of the positiion pointed at in the screen
         */
        getMouseX(): number;

        /**
         * Returns the y-coordinate of the position pointed at in the screen.
         */
        getMouseY(): number;

        /**
         * Returns the state of mouse click
         */
        getMouseState(): Screen.MouseState;

        /**
         * Clear the screen
         */
        clear(): void;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        mouseDown(x: number, y: number): EventFilter;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        mouseUp(x: number, y: number): EventFilter;
    }

    /**
     * Switch
     */
    namespace Switch {
        const enum SwitchState {
            Off = 0,
            On = 1
        }
    }

    interface Switch extends Element, Toggleable, Pressable, Stateful<Switch.SwitchState> {
    }

    /**
     * System
     */
    namespace System {
        const enum ShowScreenState {
            Hide = 0,
            Show = 1
        }

        const enum MouseViewLock {
            Unlocked = 0,
            Locked = 1
        }

        const enum CharacterState {
            Unfrozen = 0,
            Frozen = 1
        }
    }

    /**
     * @noSelf
     */
    interface System {
        /**
         * Return the current key bound to the given action. Useful to display tips.
         */
        getActionKeyName(actionName: Action): string;

        /**
         * Control the display of the ontrol unit custom screen, where you can define customized display information
         * in HTML.
         * Note that this function is disabled if the player is not running the script explicitly.
         */
        showScreen(state: System.ShowScreenState): void;

        /**
         * Set the content of the control unit custom screen with some HTML code
         * Note that this function is disabled if the player is not running the script explicitly.
         */
        setScreen(content: string): void;

        /**
         * Create an empty panel
         * Note that this function is disabled if the player is not running the script explicitly.
         */
        createWidgetPanel(label: string): string;

        /**
         * Destroy the panel
         * Note that this function is disabled if the player is not running the script explicitly.
         */
        destroyWidgetPanel(panelId: string): Outcome;

        /**
         * Create an empty widget and add it to a panel.
         * Note that this function is disabled if the player is not running the script explicitly.
         */
        createWidget(panelId: string, type: WidgetType): string;

        /**
         * Destriy the widget.
         * Note that this function is disabled if the player is not running the script explicitly.
         */
        destroyWidget(widgetId: string): Outcome;

        /**
         * Create data
         * Note that this function is disabled if the player is not running the script explicitly.
         */
        createData(dataJson: Json): string;

        /**
         * destroy data
         * Note that this function is disabled if the player is not running the script explicitly.
         */
        destroyData(dataId: string): Outcome;

        /**
         * Update data
         * Note that this function is disabled if the player is not running the script explicitly.
         */
        updateData(dataId: string, dataJson: Json): Outcome;

        /**
         * Add data to a widget
         * Note that this function is disabled if the player is not running the script explicitly.
         */
        addDataToWidget(dataId: string, widgetId: string): Outcome;

        /**
         * Remove data from a widget
         * Note that this function is disabled if the player is not running the script explicitly.
         */
        removeDataFromWidget(dataId: string, widgetId: string): Outcome;

        /**
         * Return the current value of the mouse wheel
         */
        getMouseWheel(): number;

        /**
         * Return the current value of the mouse delta X
         */
        getMouseDeltaX(): number;

        /**
         * Return the current value of the mouse delta Y
         */
        getMouseDeltaY(): number;

        /**
         * Return the current value of the mouse pos X
         */
        getMousePosX(): number;

        /**
         * Return the current value of the mouse pos Y
         */
        getMousePosY(): number;

        /**
         * Returns the current value of the mouse wheel.
         * Range: (0,1)
         */
        getThrottleInputFromMouseWheel(): number;

        /**
         * Returns the mouse input for the ship control action (forward/backward)
         * Range: (-1,1)
         */
        getControlDeviceForwardInput(): number;

        /**
         * Returns the mouse input for the ship control action (yaw right/left)
         * Range: (-1,1)
         */
        getControlDeviceYawInput(): number;

        /**
         * Returns the mouse input for the ship control action (right/left)
         * Range: TODO - Range for system.getControlDeviceLeftRightInput()
         */
        getControlDeviceLeftRightInput(): number;

        /**
         * Lock or unlock the mouse free look
         * Note that this function is disabled if the player is not running the script explicitly.
         */
        lockView(state: System.MouseViewLock): void;

        /**
         * Returns the lock state of the mouse free look
         */
        isViewLocked(): System.MouseViewLock;

        /**
         * Freezes the character, liberating the associated movement keys to be used by the script.
         * Note that this function is disabled if the player is not running the script explicitly.
         */
        freeze(state: System.CharacterState): void;

        /**
         * Returns the frozen status of the character
         */
        isFrozen(): System.CharacterState;

        /**
         * Return the current time since the arrival of the Arkship
         */
        getTime(): Seconds;

        /**
         * Return the delta time of action updates (to use in action loop)
         */
        getActionUpdateDeltaTime(): Seconds;

        /**
         * Return the name of the given player, if in range of visibility.
         */
        getPlayerName(id: number): string;

        /**
         * Return the world position of the given player, if in range of visibility.
         */
        getPlayerWorldPos(id: number): Vec3Tuple;

        /**
         * Print a message to the lua console
         */
        print(msg: string): void;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        actionLoop(action: DU.Action): EventFilter;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        actionStart(action: DU.Action): EventFilter;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        actionStop(action: DU.Action): EventFilter;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        flush(): EventFilter;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        start(): EventFilter;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        stop(): EventFilter;

        /**
         * @deprecated This is an event filter and is not available on the object
         * @event
         */
        update(): EventFilter;
    }

    /**
     * Telemeter
     */
    namespace Telemeter {}
    /**
     * @noSelf
     */
    interface Telemeter extends Element {
        /**
         * Returns the distance to the first obstacle in front of the telemeter
         */
        getDistance(): number;

        /**
         * Returns the max distance from which an obstacle can be detected
         */
        getMaxDistance(): number;
    }

    namespace WarpDrive {
        interface Data {
            buttonMsg: string;    // TODO WarpDrive.Data.buttonMsg values: "CANNOT WARP" | ...
            cellCount: string;
            destination: string;
            distance: number;
            elementId: string;
            errorMsg: string;   // TODO WarpDrive.Data.errorMsg values: "NO WARP CONTAINER" | ...
            helperId: "warpdrive";
            name: string;
            showError: boolean;
            type: "warpdrive";
        }
    }

    interface WarpDrive extends Element {
        /**
         * Start the warp drive, if a warp destination has been selected
         */
        activateWarp(): void;
    }


    namespace Weapon {

        interface Data {
            elementId: string;
            helperId: WidgetType.Weapon;
            name: string;
            properties: {
                ammoCount: number;
                ammoMax: number;
                ammoName: string;   // TODO Weapon.Data.properties.ammoName values: "" | ...
                ammoTypeId: string; // "0"
                cycleAnimationRemainingTime: number;
                fireBlocked: boolean;
                fireCounter: number;
                fireReady: boolean;
                hitProbability: number;
                hitResult: number;      // TODO hitResult enum?
                isBroken: boolean;
                outOfZone: boolean;
                repeatedFire: boolean;
                weaponStatus: number;   // TODO status enum?
            };
            staticProperties: {
                baseDamage: number;
                cycleTime: number;
                magazineVolume: number;
                optimalAimingCone: number;
                optimalDistance: number;
                optimalTracking: number;
                reloadTime: number;
                size: "xs" | "s" | "m" | "l";
                unloadTime: number;
            }
            targetConstruct: { constructId: string };
            type: WidgetType.Weapon;
        }
    }

    interface Weapon extends Element<DU.WidgetType.Weapon> {

    }
}
