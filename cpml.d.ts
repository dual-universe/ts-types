/**
 * @hidden
 * @noResolution
 */
declare module "cpml.constants" {
    import * as _ from "cpml/constants";
    export = _;
}
/**
* @noResolution
 */
declare module "cpml/constants" {
    export const deg2rad: number;
    export const rad2deg: number;
    export const epsilon: number;
    export const FLT_EPSILON: number;
    export const DOT_THRESHOLD: number;
}
/**
 * @hidden
 * @noResolution
 */
declare module "cpml.vec2" {
    import * as _ from "cpml/vec2";
    export = _;
}

/**
* @noResolution
 */
declare module "cpml/vec2" {
    type Vec2Props = {
        x: number;
        y: number;
    };
    namespace vec2 {
        interface Vec2Methods {
            clone(): Vector;
            /** @tupleReturn **/
            unpack(): [number, number, number];
            dot(b: Vector): number;
            len2(): number;
            len(): number;
            dist(b: Vector): number;
            dist2(b: Vector): number;
            normalize_inplace(): Vector;
            normalize(): Vector;
            rotate_inplace(phi: number): Vector;
            rotate(phi: number, axis: Vector): Vector;
            perpendicular(): Vector;
            project_on(vec: Vector): Vector;
            project_on_plane(vec: Vector): Vector;
            project_from(vec: Vector): Vector;
            mirror_on(vec: Vector): Vector;
            cross(vec: Vector): Vector;
            trim_inplace(maxLen: number): Vector;
            angle_to(other: Vector): number;
            trim(maxLen: number): Vector;
            lerp(b: Vector, s: number): Vector;
        }
        type Vector = number & Vec2Props & Vec2Methods;
    }
    type Vec2Like = Vec2Props | [number, number];
    type Ctor1 = (x: number, y: number) => vec2.Vector;
    type Ctor2 = (vec2: Vec2Like) => vec2.Vector;
    type Vec2Ctor = Ctor1 & Ctor2;
    const vec2: Vec2Ctor & {
        new: Vec2Ctor;
        lerp: vec2.Vector["lerp"];
        isvector(v: any): v is vec2.Vector;
        zero: vec2.Vector;
        unit_x: vec2.Vector;
        unit_y: vec2.Vector;
    };
    export = vec2;
}
/**
 * @hidden
 * @noResolution
 */
declare module "cpml.vec3" {
    import * as _ from "cpml/vec3";
    export = _;
}
/**
* @noResolution
 */
declare module "cpml/vec3" {
    import * as Vec2 from "cpml/vec2";
    type Vec3Props = {
        x: number;
        y: number;
        z: number;
    };
    namespace vec3 {
        interface Vec3Methods {
            clone(): Vector;
            /** @tupleReturn **/
            unpack(): [number, number, number];
            dot(b: Vector): number;
            len2(): number;
            len(): number;
            dist(b: Vector): number;
            dist2(b: Vector): number;
            normalize_inplace(): Vector;
            normalize(): Vector;
            rotate(phi: number, axis: Vector): Vector;
            perpendicular(): Vector;
            project_on(vec: Vector): Vector;
            project_on_plane(vec: Vector): Vector;
            project_from(vec: Vector): Vector;
            mirror_on(vec: Vector): Vector;
            cross(vec: Vector): Vector;
            trim_inplace(maxLen: number): Vector;
            trim(maxLen: number): Vector;
            angle_to(other: Vec2.Vector): number;
            angle_between(other: Vector): number;
            orientation_to_direction(orientation: Vector): Vector;
            lerp(b: Vector, s: number): Vector;
        }
        type Vector = number & Vec3Props & Vec3Methods;
    }
    type Vec3Like = Vec3Props | [number, number, number];
    type Vec3Ctor1 = (this: void, vec?: Vec3Like) => vec3.Vector;
    type Vec3Ctor2 = (this: void, x?: number, y?: number, z?: number) => vec3.Vector;
    type Vec3Ctor = Vec3Ctor1 & Vec3Ctor2;
    const vec3: Vec3Ctor & {
        new: Vec3Ctor;
        lerp: vec3.Vector["lerp"];
        isvector(v: any): v is vec3.Vector;
        zero: vec3.Vector;
        unit_x: vec3.Vector;
        unit_y: vec3.Vector;
        unit_z: vec3.Vector;
    };
    export = vec3;
}
/**
 * @hidden
 * @noResolution
 */
declare module "cpml.quat" {
    import * as _ from "cpml/quat";
    export = _;
}
/**
* @noResolution
 */
declare module "cpml/quat" {
    import * as vec3 from "cpml/vec3";
    type QuaternionProps = {
        x: number;
        y: number;
        z: number;
        w: number;
    };
    namespace quat {
        type Quaternion = number & QuaternionProps & {
            /** @tupleReturn **/
            unpack(): [number, number, number, number];
            /** @noSelf **/
            unit(): Quaternion;
            /** @tupleReturn **/
            to_axis_angle(): [number, vec3.Vector];
            is_zero(): boolean;
            is_real(): boolean;
            is_imaginary(): boolean;
            dot(q2: Quaternion): number;
            cross(q2: Quaternion): Quaternion;
            len(): number;
            len2(): number;
            normalize(): Quaternion;
            scale(l: number): Quaternion;
            conjugate(): Quaternion;
            inverse(): Quaternion;
            reciprocal(): Quaternion;
            real(): number;
            clone(): Quaternion;
            to_vec3(): vec3.Vector;
            /** @tupleReturn **/
            to_euler(): [number, number, number];
            lerp(b: Quaternion, s: number): Quaternion;
            slerp(b: Quaternion, s: number): Quaternion;
        };
    }
    type Vec3Like = {
        x: number;
        y: number;
        z: number;
    } | [number, number, number];
    type Ctor1 = (this: void, q: QuaternionProps) => quat.Quaternion;
    type Ctor2 = (this: void, tuple: [number, number, number, number]) => quat.Quaternion;
    type Ctor3 = (this: void, x: number, y: number, z: number, w: number) => quat.Quaternion;
    type Ctor4 = (this: void, vec: Vec3Like, w: number) => quat.Quaternion;
    type Ctor = Ctor1 & Ctor2 & Ctor3 & Ctor4;
    const quat: Ctor & {
        new: Ctor;
        rotate(angle: number, axis: vec3.Vector): quat.Quaternion;
        from_direction(normal: vec3.Vector, up: vec3.Vector): quat.Quaternion;
    };
    export = quat;
}
/**
 * @hidden
 * @noResolution
 */
declare module "cpml.mat4" {
    import * as _ from "cpml/mat4";
    export = _;
}
/**
* @noResolution
 */
declare module "cpml/mat4" {
    import { Quaternion } from "cpml/quat";
    import { Vector } from "cpml/vec3";
    type Vec3Like = {
        x: number;
        y: number;
        z: number;
    };
    type Matrix16 = [number, number, number, number, number, number, number, number, number, number, number, number, number, number, number, number];
    type Matrix9 = [number, number, number, number, number, number, number, number, number];
    type Matrix4x4 = [[number, number, number, number], [number, number, number, number], [number, number, number, number], [number, number, number, number]];
    type Ctor1 = (this: void, mat: Matrix16) => mat4.Mat4;
    type Ctor2 = (this: void, mat: Matrix9) => mat4.Mat4;
    type Ctor3 = (this: void, mat: Matrix4x4) => mat4.Mat4;
    type Ctor = Ctor1 & Ctor2 & Ctor3;
    namespace mat4 {
        type Mat4 = number & Matrix16 & {
            to_quat(): Quaternion;
            ortho(left: number, right: number, top: number, bottom: number, near: number, far: number): Mat4;
            hmd_perspective(tanHalfFov: number, zNear: number, zFar: number, flipZ: boolean, farAtInfinity: boolean): Mat4;
            perspective(fovy: number, aspect: number, near: number, far: number): Mat4;
            translate(t: Vec3Like): Mat4;
            scale(s: Vec3Like): Mat4;
            rotate(quat: Quaternion): Mat4;
            rotate(angle: number, axis: Vector): Mat4;
            identity(): Mat4;
            clone(): Mat4;
            invert(): Mat4;
            look_at(eye: Vector, center: Vector, up: Vector): Mat4;
            transpose(): Mat4;
            to_vec4s(): Matrix4x4;
            project(this: void, obj: Vec3Like, view: Mat4, projection: Mat4, viewport: Mat4): Vector;
            unproject(this: void, win: Vec3Like, view: Mat4, projection: Mat4, viewport: Mat4): Vector;
            from_direction(this: void, direction: Vector, up: Vector): Mat4;
            from_transform(this: void, t: Vec3Like, rot: Quaternion, scale: number): Mat4;
        };
    }
    const mat4: Ctor & mat4.Mat4;
    export = mat4;
}
/**
 * @hidden
 * @noResolution
 */
declare module "cpml.pid" {
    import * as _ from "cpml/pid";
    export = _;
}
/**
* @noResolution
 */
declare module "cpml/pid" {
    namespace pid {
        interface PID {
            inject(value: number): void;
            get(): number;
            reset(): void;
        }
    }
    type Ctor = (this: void, p: number, i: number, d: number, amortization?: number) => pid.PID;
    const pid: Ctor & {
        new: Ctor;
    };
    export = pid;
}
/**
 * @hidden
 * @noResolution
 */
declare module "cpml.sgui" {
    import * as _ from "cpml/sgui";
    export = _;
}
/**
* @noResolution
 */
declare module "cpml/sgui" {
    namespace sgui {
        type OnClick = () => void;
        export interface ClickableArea {
            new: (this: void, x: number, y: number, hx: number, hy: number, fun: OnClick) => ClickableArea;
            contains(x: number, y: number): boolean;
        }
        /**
         * `sgui` is a global variable corresponding to an SGui instance.
         */
        export interface SGui {
            createClickableArea(id: any, x: number, y: number, hx: number, hy: number, fun: OnClick): void;
            deleteClickableArea(id: any): void;
            createButtonArea(screen: DU.Screen, x: number, y: number, hx: number, hy: number, text: string, fun: OnClick): number;
            deleteButtonArea(screen: DU.Screen, id: number): void;
            process(x: number, y: number): void;
        }
    }
    export = sgui;
}
/**
 * @hidden
 * @noResolution
 */
declare module "cpml.utils" {
    import * as _ from "cpml/utils";
    export = _;
}
/**
* @noResolution
* @noSelf
 */
declare module "cpml/utils" {
    export function clamp(value: number, min: number, max: number): number;
    export function deadzone(value: number, size: number): number;
    export function threshold(value: number, threshold: number): boolean;
    export function map(value: number, minIn: number, maxIn: number, minOut: number, maxOut: number): number;
    export function lerp(progress: number, low: number, high: number): number;
    export function smoothstep(progress: number, low: number, high: number): number;
    export function round(value: number, precision: number): number;
    export function wrap(value: number, limit: number): number;
    export function is_pot(value: number): boolean;
    export function pow2(value: number): number;
}
