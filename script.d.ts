/**
 * hdparm's wrap.lua script object
 *
 * @noSelf
 */
/**
 * @noSelf
 */
declare interface Script<Timers = string, Channels = string> {
    /**
     * Run on Unit Start.
     * must be called in your main file.
     */
    onStart(): void;
    /**
     * Run on Unit Stop.
     */
    onStop?(): void;
    /**
     * Run on timer tick
     */
    onTick?(timer: Timers): void;
    /**
     * Run on industry complete
     */
    onCompleted?(): void;
    /**
     * Run on industry status change
     */
    onStatusChanged?(status: DU.Industry.IndustryStatus): void;
    /**
     * Run on screen mouse down
     */
    onMouseDown?(x: number, y: number): void;
    /**
     * Run on screen mouse up
     */
    onMouseUp?(x: number, y: number): void;
    /**
     * Run on laser detector hit
     */
    onLaserHit?(): void;
    /**
     * Run on laser detector release
     */
    onLaserRelease?(): void;
    /**
     * Run on Emitter receive
     */
    onReceive?(channel: Channels, message: string): void;
    /**
     * Run on Detection Zone enter
     */
    onEnter?(id: number): void;
    /**
     * Run on Detection Zone leave
     */
    onLeave?(id: number): void;
    /**
     * Run on switch/pressure tile press
     */
    onPressed?(): void;
    /**
     * Run on switch/pressure tile release
     */
    onReleased?(): void;
    /**
     * Run on System actionStart
     */
    onActionStart?(action: DU.Action): void;
    /**
     * Run on System actionStop
     */
    onActionStop?(action: DU.Action): void;
    /**
     * Run on System actionLoop
     */
    onActionLoop?(action: DU.Action): void;
    /**
     * Run on System update
     */
    onUpdate?(): void;
    /**
     * Run on System flush
     */
    onFlush?(): void;
}
